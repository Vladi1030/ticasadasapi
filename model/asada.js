const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const asada = new Schema({
    nombre: {
        type: String,
        required: true
    },
    direccion: {
        type: String,
        required: true,
        unique: true
    },
    correo: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('asada', asada);
