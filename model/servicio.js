const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const servicio = new Schema({
    nombre: {
        type: String,
        required: true
    },
    precio: {
        type: Number,
        required: true
    },
    unidad: {
        type: String,
        required: true
    },
    impuesto: {
        type: Number,
        required: true
    },
    idAsada: {
        type: Schema.Types.ObjectId,
        ref: "asada",
        required: true
    }
});

module.exports = mongoose.model('servicio', servicio);
