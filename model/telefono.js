const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const telefono = new Schema({
    numero: {
        type: String,
        required: true
    },
    idAsada: {
        type: Schema.Types.ObjectId,
        ref: "asada",
        required: true
    }
});

module.exports = mongoose.model('telefono', telefono);
