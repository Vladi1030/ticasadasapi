const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const usuario = new Schema({
    nombre: {
        type: String,
        required: true
    },
    apellido: {
        type: String,
        required: true
    },
    cedula: {
        type: String,
        required: true
    },
    nis: {
        type: String,
        required: true
    },
    telefono: {
        type: Number,
        required: true
    },
    direccion: {
        type: String,
        required: true
    },
    correo: {
        type: String,
        required: true
    },
    contrasenna: {
        type: String,
        required: true
    },
    role: { // clienteAsada, cobrador(asadaAdmin), recolector(del consumo de cada cliente), admin(Vladi)
        type: Date,
        required: true
    },
    idAsada: {
        type: Schema.Types.ObjectId,
        ref: "asada",
        required: true
    }
});

module.exports = mongoose.model('usuario', usuario);