const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const mensaje = new Schema({
    titulo: {
        type: String,
        required: true
    },
    detalle: {
        type: Number,
        required: true
    },
    fechaInicio: {
        type: Date,
        required: true
    },
    fechaVencimiento: {
        type: Date,
        required: true
    },
    idAsada: {
        type: Schema.Types.ObjectId,
        ref: "asada",
        required: true
    }
});

module.exports = mongoose.model('mensaje', mensaje);