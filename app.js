/* En esta sección se importa todo lo requirido para el
funcionamiento del proyecto */
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const app = express();
const cors = require('cors');
const jwt = require('jsonwebtoken');
const config = require('./config');
const bodyParser = require('body-parser');
const btoa = require('btoa');
const atob = require('atob');

/* En esta sección se importan los modelos necesarios */
const Asada = require('./model/asada');
/* const Factura = require('./model/factura');
const Mensaje = require('./model/mensaje');
const Servicio = require('./model/servicio');
const Telefono = require('./model/telefono');
const Usuario = require('./model/usuario');*/

/* Puerto del API */
const port = process.env.PORT || 3000;

/* Conexión a la base de datos */
const db = mongoose.connect(config.database, {
    useNewUrlParser: true
});

/* Se declara lo que el API va a utilizar */
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(express.static(__dirname + '/public'));
app.use(cors({
    origin: '*'
}));
app.all('*', cors());

/* Mensaje para el arranque del API */
app.listen(3000, () => console.log('El API de Ticasada está escuchando en el puerto 3000!'));

/* Mensaje para la petición de erronea */
app.get('/', function(req, res) {
    res.send('Hola ahí! El API está funcionando en http://localhost:' + port + '/api');
});

/* Mensaje para la petición de erronea sin sección */
app.get('/api/', function(req, res) {
    res.send('Hola ahí! Por favor, especifique la sección http://localhost:' + port + '/api' + '/seccion');
});

/*  En este método se agregan nuevas asadas
    Este método recibe todos los datos de las asadas
*/
app.post('/api/asadas', (req, res) => {
    let asada = new Asada();

    asada.nombre = req.body.nombre;
    asada.direccion = req.body.direccion;
    asada.correo = req.body.correo;

    if (asada.nombre === '' || asada.direccion === '' || asada.correo === '') {
        res.status(400);
        res.json("Por favor, envíe toda la información requerida!");
        return;
    }

    Asada.findOne({
        nombre: asada.nombre,
        correo: asada.correo
    }, function(err, foundasada) {
        if (foundasada != null) {
            res.status(400);
            res.json("Ya existe una asada con este nombre y correo");
            return;
        } else {
            asada.save(function(err) {
                if (err) {
                    res.status(400);
                    res.json({
                        error: err
                    });
                    return;
                }
                res.status(201);
                res.json(user);
            });
            
        }
    });
});

/* Este método retorna las asadas existentes en el sistema */
app.get('/api/asadas', (req, res) => {
    Asada.find(function(err, asadas) {
        if (err) {
            res.status(400);
            res.send(err);
        }
        res.status(200);
        res.json(asadas);
    });
});

/* Retorna la asada que esté registrada con el id recibido */
app.get('/api/asadas/:id', function(req, res) {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('El token no es valido!');
        return;
    }

    if (req.params.id === null) {
        res.send("Error de parametro nulo!");
    } else {
        if (mongoose.Types.ObjectId.isValid(req.params.id)) {
            Asada.findById(req.params.id, function(err, asadas) {
                if (err) {
                    res.status(404);
                    res.send("No se ha encontrado la asada solicitada!");
                }
                res.status(200);
                res.send(asadas);
            })
        }
    }
})

/* Este método elimina la asada asociada al id recibido */
app.delete('/api/asadas/:id', (req, res) => {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('El token no es valido!');
        return;
    }

    if (req.params.id === null) {
        res.status(400);
        res.send("Error de parametro nulo!");
    } else {
        if (mongoose.Types.ObjectId.isValid(req.params.id)) {
            Asada.findOneAndRemove({
                _id: req.params.id
            }, (err) => {
                if (err) {
                    res.status(404);
                    res.send("No se ha encontrado la asada solicitada!");
                }
                res.status(200);
                res.json("Asada eliminada!");
            });
        }
    }
});

/* Actualiza la asada asociada a el id recibido con los datos recibidos */
app.patch('/api/asadas/:id', (req, res) => {
    let token = req.headers['authorization'];
    if (!verifyToken(token)) {
        res.status(401);
        res.json('Token invalido!');
        return;
    }

    if (req.params.id === null) {
        res.status(400);
        res.send("Error de parametro nulo!");
    } else {
        if (mongoose.Types.ObjectId.isValid(req.params.id)) {
            Asada.findOneAndUpdate({
                _id: req.params.id
            }, {
                $set: req.body.newinfo
            }, {
                new: true
            }, function(err, user) {
                if (err) {
                    res.status(400);
                    res.send("Sucedio un error mientras se actualizaba la asada!");
                }
                res.status(201);
                res.send("Datos actualizados!");
            });
        }
    }
});

/*
//Auth user
app.post('/api/authenticate', function(req, res) {

    // find the user
    User.findOne({
        email: req.body.email
    }, function(err, user) {

        if (err) throw err;

        if (!user) {
            res.status(400);
            res.json({
                success: false,
                message: 'Authentication failed. User not found.'
            });
        } else if (user) {
            if (user.verified) {
                // check if password matches
                if (user.password != btoa(req.body.password)) {
                    res.status(400);
                    res.json({
                        success: false,
                        message: 'Authentication failed. Wrong information.'
                    });
                } else {

                    // if user is found and password is right
                    // create a token with only our given payload
                    // we don't want to pass in the entire user since that has the password
                    let payload = {
                        id: user._id,
                        email: user.email,
                        password: user.password,
                        name: user.name,
                        lastname: user.lastname,
                        country: user.country,
                        birthday: user.birthday,
                        user_type: 'user'
                    };

                    var token = jwt.sign(payload, config.secret, {
                        expiresIn: 86400
                    });

                    // return the information including token as JSON
                    res.status(200);
                    res.json({
                        success: true,
                        user_type: "user",
                        message: 'Enjoy your token!',
                        token: token
                    });
                }
            } else {
                res.status(400);
                res.json('You need to verify your e-mail address first, check out your email!');
            }
        }

    });
});

//Verify
app.post('/api/users/verify', function(req, res) {
    let senttoken = req.body.verifytoken;
    let password_from = senttoken.split("zxc.23").pop();
    let email_from = atob(senttoken.split('zxc.23').shift());

    // find the user
    User.findOne({
        email: email_from
    }, function(err, user) {

        if (err) throw err;

        if (!user) {
            res.status(400);
            res.json({
                success: false,
                message: 'Authentication failed. User not found.'
            });
        } else if (user) {

            // check if password matches
            if (user.password != password_from) {
                res.status(400);
                res.json({
                    success: false,
                    message: 'Verification failed. Wrong information.'
                });
            } else {
                user.verified = true;

                user.save(function(err, verifiedUser) {
                    if (err) return handleError(err);
                    user = verifiedUser;
                });

                let payload = {
                    id: user._id,
                    email: user.email,
                    password: user.password,
                    name: user.name,
                    lastname: user.lastname,
                    country: user.country,
                    birthday: user.birthday,
                    verified: user.verified,
                    user_type: 'user'
                };

                var token = jwt.sign(payload, config.secret, {
                    expiresIn: 86400
                });

                // return the information including token as JSON
                res.status(200);
                res.json({
                    success: true,
                    user_type: "user",
                    message: 'Enjoy your token!',
                    token: token
                });
            }
        }
    });
});

*/

/* Verifica el token con el correos */
var verifyToken = function(token) {
    let verified = false;
    if (!token) {
        verified = false;
    }

    token = token.replace('Bearer ', '');

    jwt.verify(token, 'tubekidsapi', function(err) {
        if (err) {
            verified = false;
        } else {
            verified = true;
        }
    });

    return verified;
};
